import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() logged;

  @Output()
  logoutEvent = new EventEmitter<any>()
  constructor(public router: Router) { }

  ngOnInit() {
  }

  handleLogout() {
    this.logoutEvent.emit("logout")
  }

  ngOnDestroy(): void {
  }

}
