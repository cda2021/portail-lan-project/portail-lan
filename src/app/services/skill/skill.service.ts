import { Injectable } from '@angular/core';
import { prepareHeaders } from "../../../functions/headers";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  skills: Array<{_id: string}>;
  constructor(private httpClient: HttpClient) { }

  getSkill() {
    return fetch("http://127.0.0.1:5000/api/skills", {
      method: "GET",
      headers: prepareHeaders()});
  }

  addSkill(name) {
    const reqHeaders = prepareHeaders();
    return fetch("http://127.0.0.1:5000/api/skills", {
      method: 'POST',
      body: JSON.stringify({"name": name}),
      headers: reqHeaders,
      redirect: 'follow'
    });
  }

  getGraduation()
  {
    return fetch("http://127.0.0.1:5000/api/evaluates", {
      method: "GET",
      headers: prepareHeaders()});
  }

  addGraduation(skill, student, statut){
    const user = localStorage.getItem("user-id");
    const mydate = new Date();

    return fetch("http://127.0.0.1:5000/api/evaluates", {
      method: 'POST',
      body: JSON.stringify({'ref_skill': skill, 'ref_student': student, 'ref_instructor': user, 'graduation': statut, 'graduated_at': mydate.getTime()}),
      headers: prepareHeaders,
      redirect: 'follow'
    });

  }
}
