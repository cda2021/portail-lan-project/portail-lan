import { Injectable } from '@angular/core';
import { prepareHeaders } from "../../../functions/headers";
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private emitChangeSource = new Subject<any>();
  changeEmitted$ = this.emitChangeSource.asObservable();

  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  login(credentials: any): Promise<any> {
    return this.authenticate(credentials)
  }

  async authenticate({username, password}) {
    let reqHeaders = prepareHeaders();

    return fetch("http://127.0.0.1:5000/login", {
      method: 'POST',
      body: JSON.stringify({"email": username.value, "password": password.value}),
      headers: reqHeaders,
      redirect: 'follow'
    });
  }
}
