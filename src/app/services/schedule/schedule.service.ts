import { Injectable } from '@angular/core';
import { preparePostParams, prepareGetParams } from "../../../functions/headers";

@Injectable({
  providedIn: 'root'
})
export class ScheduleService {

  constructor() { }

  createCourse(name: string, date_course: number, scheduleId: string, ref_skill: string = null) {
    let body = {
      name,
      date_course
    };

    if (ref_skill !== null) {
      // @ts-ignore
      body.ref_skill = ref_skill;
    }

    return fetch(`http://localhost:5000/api/schedules/${scheduleId}/add`, {...preparePostParams(), body: JSON.stringify(body)});
  }

  all() {
    return fetch("http://localhost:5000/api/schedules", {...prepareGetParams()})
  }

  addCourse(scheduleId: string, courseId) {
    const body = JSON.stringify({
      id: courseId
    })

    return fetch(`http://localhost:5000/api/schedules/${scheduleId}`, {...preparePostParams(), body})
  }

  new() {
    const body =  JSON.stringify({ref_list_cours: []});
    return fetch("http://localhost:5000/api/schedules", {...preparePostParams(), body}).then((res => res.json()))
  }
}
