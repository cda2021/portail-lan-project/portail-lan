import { Injectable } from '@angular/core';
import { preparePostParams, prepareGetParams } from "../../../functions/headers";
import { prepareHeaders } from "../../../functions/headers";
import {ScheduleService} from "../schedule/schedule.service";

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private scheduleService: ScheduleService) { }

  new(name: string, list_student: [string], ref_schedule: string) {
    const body = JSON.stringify({
      name,
      list_student,
      ref_schedule
    })

    return fetch("http://localhost:5000/api/promotions", {...preparePostParams(), body});
  }

  all() {
    return fetch("http://localhost:5000/api/promotions", {...prepareGetParams()})
  }

  newWithoutSchedule(promotion) {
    return new Promise((resolve, reject) => {
      this.scheduleService.new().then((result) => {
        promotion.ref_schedule = result.id;
        const body = JSON.stringify(promotion);
        fetch("http://localhost:5000/api/promotions", {...preparePostParams(), body}).then(res => res.json()).then((result) => {
          resolve(result)
        });
      })
    })
  }

  get(id: string) {
    return fetch(`http://localhost:5000/api/promotions/${id}`, prepareGetParams());
  }
}
