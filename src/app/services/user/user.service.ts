import { Injectable } from '@angular/core';
import { prepareGetParams, preparePostParams, prepareDeleteParams } from '../../../functions/headers';
import {CompanyService} from "../company/company.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private companyService: CompanyService) { }

  all() {
    return fetch("http://localhost:5000/api/users", prepareGetParams()).then((result => result.json()))
  }

  async registerStudents(users: []) {
    return Promise.all((users.map((user: any) => {
      let userId;
      let trainerId;

      this.register(user.user).then((result) => {
        userId = result.id;
        this.register(user.trainer).then((result) => {
          trainerId = result.id;
        }).then(() => {
          let company = user.company;
          company.ref_student = userId;
          company.ref_training_supervisor = trainerId;
          this.companyService.new(company).then((result) => console.log(result));
        })
      })
    })))
  }

  register(user) {
    return fetch("http://localhost:5000/api/users", {...preparePostParams(), body: JSON.stringify(user)}).then((res => res.json()))
  }

  delete(id) {
    return fetch(`http://localhost:5000/api/users/${id}`, prepareDeleteParams()).then((res => res.json()))
  }
}
