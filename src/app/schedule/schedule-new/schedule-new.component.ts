import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ScheduleService} from "../../services/schedule/schedule.service";
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-schedule-new',
  templateUrl: './schedule-new.component.html',
  styleUrls: ['./schedule-new.component.scss']
})
export class ScheduleNewComponent implements OnInit {

  @Input() promotions;
  // @ts-ignore
  @ViewChild('formElement') form:ElementRef;
  error = false;
  public scheduleForm = new FormGroup({
    schedule: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required])
  });
  minDate = new Date().getFullYear();

  constructor(private scheduleService: ScheduleService, private modalController: ModalController) { }

  ngOnInit() {
    this.form.nativeElement.addEventListener('keypress', (event) => {
      if (event.key === 'Enter') {
        this.handleSubmit(event);
      }
    })
  }

  handleSubmit($event) {
    $event.preventDefault();

    if (this.scheduleForm.valid) {
      let controls = this.scheduleForm.controls;
      let date = new Date(controls.date.value).getTime();
      this.scheduleService.createCourse(controls.name.value, date, controls.schedule.value)
        .then((response => {
          if (response.status === 400) {
            this.error = true;
            return null;
          }
          return response.json();
        }))
        .then((result) => {
          console.log("result", result)
          this.modalController.dismiss();
        })
    } else {
      this.error = true;
    }
  }
}
