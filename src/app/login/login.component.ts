import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {AuthenticationService} from "../services/authentication/authentication.service";
import {Router} from "@angular/router";
import {LoadingController} from "@ionic/angular";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public errors = null;
  public loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  public loading: any;

  constructor(
    public authService: AuthenticationService,
    public router: Router,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    const that = this;
    this.presentLoading();

    document.querySelector('form').addEventListener('keypress', (event) => {
      if (event.key === 'Enter') {
        that.handleSubmit();
      }
    })
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
  }

  handleSubmit() {
    this.loading.present();
    this.authService.login(this.loginForm.controls)
      .then(response => response.json())
      .then((result) => {
        this.loading.dismiss();
        if (result.meta.code === 200) {
          localStorage.setItem('user-id', result.response.user.id)
          localStorage.setItem('authentication-token', result.response.user.authentication_token)
          this.authService.emitChange("login")
          this.router.navigate(['/dashboard'])
        }

        this.presentLoading();

        if (result.meta.code === 400) {
          console.log(result)
          this.errors = result.response.errors;
        }
      })
      .catch((error) => {
        console.log(error)
      });
  }
}
