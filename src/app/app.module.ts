import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ɵs } from '@ng-select/ng-select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicModule } from '@ionic/angular';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { ScheduleEditComponent } from './schedule/schedule-edit/schedule-edit.component';
import { ScheduleNewComponent } from './schedule/schedule-new/schedule-new.component';
import { HeaderComponent } from './header/header.component';
import { SkillNewComponent } from './skill/skill-new/skill-new.component';
import { SkillGraduationComponent } from './skill/skill-graduation/skill-graduation.component';
import { UsersComponent } from './users/users.component';
import { UsersNewComponent } from './users/users-new/users-new.component';
import { UsersNewFormComponent } from './users/users-new/users-new-form/users-new-form.component';
import { PromotionsComponent } from './promotions/promotions.component';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { PromotionsNewComponent } from './promotions/promotions-new/promotions-new.component';
import { PromotionsEditComponent } from './promotions/promotions-edit/promotions-edit.component';
import {NgSelectModule} from "@ng-select/ng-select";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AppComponent,
    LoginComponent,
    ScheduleEditComponent,
    ScheduleNewComponent,
    HeaderComponent,
    UsersComponent,
    UsersNewComponent,
    UsersNewFormComponent,
    SkillNewComponent,
    SkillGraduationComponent,
    PromotionsComponent,
    NavigationBarComponent,
    PromotionsNewComponent,
    PromotionsEditComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IonicModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgSelectModule,

  ],
  providers: [
    NgSelectConfig,
    ɵs
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ScheduleNewComponent
  ]
})
export class AppModule { }
