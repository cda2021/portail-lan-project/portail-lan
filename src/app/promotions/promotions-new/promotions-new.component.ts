import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {NgSelectConfig} from "@ng-select/ng-select";
import {NgSelectComponent} from "@ng-select/ng-select";
import {FormControl, FormGroup} from "@angular/forms";
import {PromotionService} from "../../services/promotion/promotion.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-promotions-new',
  templateUrl: './promotions-new.component.html',
  styleUrls: ['./promotions-new.component.scss']
})
export class PromotionsNewComponent implements OnInit {

  users = [];
  formGroup = new FormGroup({
    name: new FormControl("")
  });
  selectedUsers = [];

  constructor (
    private userService: UserService,
    private config: NgSelectConfig,
    private promotionService: PromotionService, private router: Router
  ) {}

  ngOnInit() {
    this.userService.all().then((result) => {
      this.users = result.filter((user) => user.role === 'student');
    })
  }

  handleSubmit() {
    let promotion = {name: this.formGroup.getRawValue().name, list_student: this.selectedUsers};
    this.promotionService.newWithoutSchedule(promotion).then((result) => {
      this.router.navigate(['/promotions']);
    })
  }

  getSelectedUsers() {
    return this.users.filter((user => this.selectedUsers.includes(user.id) === true))
  }
}
