import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {PromotionService} from "../../services/promotion/promotion.service";
import {UserService} from "../../services/user/user.service";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-promotions-edit',
  templateUrl: './promotions-edit.component.html',
  styleUrls: ['./promotions-edit.component.scss']
})
export class PromotionsEditComponent implements OnInit {
  private promotion: any;
  private users = [];
  formGroup = new FormGroup({
    name: new FormControl(this.promotion ? this.promotion.name : '')
  });

  selectedUsers = [];

  constructor(
    private route: ActivatedRoute,
    private promotionService: PromotionService,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.params['id'];

    this.promotionService.get(id)
      .then((res) => res.json())
      .then((promotion) => {
        this.promotion = promotion;
        this.formGroup.controls.name.setValue(promotion.name)
        this.promotion.ref_list_student.forEach((user) => user.selected = true )
        this.userService.all().then((result) => {
          this.users = result.filter((user) => user.role === 'student');
        })
      })
      .catch((error) => {
        this.router.navigate(['/promotions'])
        console.log("error", error)
      })
  }

  handleSubmit() {
  }

  getSelectedUsers() {
    return this.users.filter((user => this.selectedUsers.includes(user.id) === true))
  }
}
