import { Component, OnInit } from '@angular/core';
import {PromotionService} from "../services/promotion/promotion.service";
import {Router} from "@angular/router";
import {UserService} from "../services/user/user.service";

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.scss']
})
export class PromotionsComponent implements OnInit {
  promotions = [];
  users = [];

  constructor(private promotionService: PromotionService, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.promotionService.all().then(res => res.json()).then((result) => {
      this.promotions = result;
    })
  }

  handleEdit(id) {
    this.router.navigate(['/promotions', id, 'edit'])
  }
}
