import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm} from "@angular/forms";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {SkillService} from "../../services/skill/skill.service";
import {Router} from "@angular/router";
import {ModalController} from "@ionic/angular";

@Component({
  selector: 'app-skill-new',
  templateUrl: './skill-new.component.html',
  styleUrls: ['./skill-new.component.scss']
})
export class SkillNewComponent implements OnInit {

  public errors = null;
  skillGroup = new FormGroup({
    name: new FormControl('')
  });

  constructor(
    public skillService: SkillService,
    public router: Router,
    public modal: ModalController) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {

    const skill = form.value['name'];
    this.skillService.addSkill(this.skillGroup.controls.name.value)
      .then(response => response.json())
      .then((result) => {
          this.modal.dismiss();
          this.router.navigate(['/dashboard']);
      })
      .catch((error) => {
        console.log(error);
      });
  }

}
