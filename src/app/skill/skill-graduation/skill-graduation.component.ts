import { Component, OnInit } from '@angular/core';
import {SkillService} from "../../services/skill/skill.service";
import {PromotionService} from "../../services/promotion/promotion.service";
import {FormControl, FormGroup, NgForm} from "@angular/forms";
import {ModalController} from "@ionic/angular";
import {Router} from "@angular/router";


@Component({
  selector: 'app-skill-graduation',
  templateUrl: './skill-graduation.component.html',
  styleUrls: ['./skill-graduation.component.scss']
})
export class SkillGraduationComponent implements OnInit {

  graduationGroup = new FormGroup({
    skill: new FormControl(""),
    promo: new FormControl(""),
    student: new FormControl(""),
    statut: new FormControl("")
  });
  constructor(public skillService: SkillService,
              public promotionService: PromotionService,
              public modal: ModalController,
              public router: Router) { }
  skillList: any;
  skillSelected = null;
  promotionList: any;
  promotionSelected = null;
  studentList: any;


  ngOnInit(){
    this.skillService.getSkill()
      .then(response => response.json())
      .then((result: any) => {
        this.skillList = result;
        this.skillSelected = this.skillList[0];
      });


    this.promotionService.all()
      .then(response => response.json())
      .then((result: any) => {
        this.promotionList = result;
        this.promotionSelected = this.promotionList[0];
      });
  }

  onSubmit(form: NgForm){
    let evaluate = this.graduationGroup.getRawValue();
    const skill = evaluate.skill;
    const student = evaluate.student;
    const statut = evaluate.statut;

    this.skillService.addGraduation(skill,student,statut)
      .then(response => response.json())
      .then((result) => {
        this.modal.dismiss();
        this.router.navigate(['/dashboard']);
      })
      .catch((error) => {
        console.log(error);
      });

  }

  changePromo($event){
    this.studentList = null;
    console.log($event.target.value);
    this.promotionList.forEach((value) =>
    {
      if (value.id === $event.target.value){
        console.log(value);
        console.log(value.ref_list_student);
        this.studentList = value.ref_list_student;
      }
    });
  }

}
