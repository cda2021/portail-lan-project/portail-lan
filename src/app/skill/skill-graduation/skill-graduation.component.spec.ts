import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillGraduationComponent } from './skill-graduation.component';

describe('SkillGraduationComponent', () => {
  let component: SkillGraduationComponent;
  let fixture: ComponentFixture<SkillGraduationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillGraduationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillGraduationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
