import { Component, OnInit } from '@angular/core';
import {ModalController} from "@ionic/angular";
import {ScheduleNewComponent} from "../schedule/schedule-new/schedule-new.component";
import {ScheduleService} from "../services/schedule/schedule.service";
import {PromotionService} from "../services/promotion/promotion.service";
import {SkillNewComponent} from "../skill/skill-new/skill-new.component";
import {SkillGraduationComponent} from "../skill/skill-graduation/skill-graduation.component";
import {SkillService} from "../services/skill/skill.service";
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(public modalController: ModalController,private scheduleService: ScheduleService, private promotionService: PromotionService, private skillService: SkillService) { }
  promotions: any;
  selectedPromotion = null;
  promotionControl = new FormControl();
  graduationList = [];

  ngOnInit() {
     this.promotionService.all()
      .then(response => response.json())
      .then((result: any) => {
        this.promotions = result;
        this.selectedPromotion = this.promotions[0];
      });

     this.skillService.getGraduation()
       .then(response => response.json())
       .then((result: any) => {

         result.forEach((value) => {
           console.log(value.ref_student.id);
           console.log( localStorage.getItem("user-id"));
           if (value.ref_student.id == localStorage.getItem("user-id")) {
             console.log("cocuocu");
             this.graduationList.push(value);
             console.log(this.graduationList);
           }
         });

       });



  }

  handleScheduleChange($event) {
    this.selectedPromotion = this.promotions.find((promo) => {
      return promo.id === this.promotionControl.value;
    })
    //this.selectedPromotion = this.promotions.find()
  }

  async handleNewCourse() {
    const modal = await this.modalController.create({
      component: ScheduleNewComponent,
      componentProps: {
        promotions: this.promotions
      },
      cssClass: 'schedule-new'
    })

    return await modal.present();
  }

  getCourses() {
    if (undefined !== this.promotions && undefined !== this.selectedPromotion) {
      const promotion = this.promotions.find((promotion) => {
        return promotion["id"] === this.selectedPromotion.id;
      });

      return promotion["ref_schedule"]["ref_list_cours"];
    }

    return [];
  }

  async newSkill() {
    const modal = await this.modalController.create({
      component: SkillNewComponent,
      componentProps: {
      },
      cssClass: 'skill-new'
    })

    return await modal.present();
  }

  async evaluteSkill() {
    const modal = await this.modalController.create({
      component: SkillGraduationComponent,
      componentProps: {
      },
      cssClass: 'skill-graduation'
    })

    return await modal.present();
  }
}
