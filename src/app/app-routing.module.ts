import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {ScheduleEditComponent} from "./schedule/schedule-edit/schedule-edit.component";
import {ScheduleNewComponent} from "./schedule/schedule-new/schedule-new.component";
import {SkillNewComponent} from "./skill/skill-new/skill-new.component";
import {SkillGraduationComponent} from "./skill/skill-graduation/skill-graduation.component";
import {UsersComponent} from "./users/users.component";
import {UsersNewComponent} from "./users/users-new/users-new.component";
import {PromotionsComponent} from "./promotions/promotions.component";
import {PromotionsNewComponent} from "./promotions/promotions-new/promotions-new.component";
import {PromotionsEditComponent} from "./promotions/promotions-edit/promotions-edit.component";


const routes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'login', component: LoginComponent },
  { path: 'schedule/:id/edit', component: ScheduleEditComponent},
  { path: 'schedule/new', component: ScheduleNewComponent},
  { path: 'skill/new', component: SkillNewComponent},
  { path: 'skill/graduation', component: SkillGraduationComponent},
  { path: 'users', component: UsersComponent },
  { path: 'promotions', component: PromotionsComponent },
  { path: 'promotions/new', component: PromotionsNewComponent },
  { path: 'promotions/:id/edit', component: PromotionsEditComponent },
  { path: 'users/new', component: UsersNewComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
