import {Component, OnInit} from '@angular/core';
import { prepareAuthenticatedHeaders } from "../functions/headers";
import {Router} from "@angular/router";
import {AuthenticationService} from "./services/authentication/authentication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'portail-lan';
  logged = false;

  constructor(public router: Router, private authService: AuthenticationService) {
    authService.changeEmitted$.subscribe(() => {
      this.logged = true;
    })
  }

  discover() {
    let reqHeaders = prepareAuthenticatedHeaders();

    fetch("http://localhost:5000/discover", {method: "GET", headers: reqHeaders, redirect: "manual"})
      .then((res) => {
        if (res.status !== 200) {
          this.router.navigate(['/login'])
        }
      })
      .then((result) => {
        this.logged = true;
      })
      .catch((reason => {
        console.log("Discover error", reason)
        this.logged = false;
        this.router.navigate(['/login'])
      }))
  }

  ngOnInit(): void {
    this.discover()
  }

  handleLogout() {
    this.logged = false;
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
