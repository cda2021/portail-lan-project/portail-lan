import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-users-new-form',
  templateUrl: './users-new-form.component.html',
  styleUrls: ['./users-new-form.component.scss']
})
export class UsersNewFormComponent implements OnInit {
  invalid: boolean = false;
 @Input() formIndex;
 @Output() changeEvent = new EventEmitter<any>();
 @Output() deleteEvent = new EventEmitter<any>();

 formGroups = {
   formGroup : new FormGroup({
     email: new FormControl('@aforp.eu', [Validators.email, Validators.required]),
     password: new FormControl(this.generateRandomString(), [Validators.required]),
     firstname: new FormControl('', [Validators.required]),
     lastname: new FormControl('', [Validators.required]),
     active: new FormControl(true)
   }),
   trainerFormGroup : new FormGroup({
     email: new FormControl('', [Validators.email]),
     password: new FormControl(this.generateRandomString(), [Validators.required]),
     firstname: new FormControl('', [Validators.required]),
     lastname: new FormControl('', [Validators.required]),
     active: new FormControl(true),
     role: new FormControl('trainer')
   }),
   companyFormGroup : new FormGroup({
     name: new FormControl("", [Validators.required]),
     siret: new FormControl("", [Validators.required]),
     address: new FormControl("", [Validators.required])
   })
 }

  constructor() { }

  ngOnInit() {
   this.formGroups.formGroup.valueChanges.subscribe(() => {
     this.handleChange();
   })
   this.formGroups.trainerFormGroup.valueChanges.subscribe(() => {
     this.handleChange();
   })
   this.formGroups.companyFormGroup.valueChanges.subscribe(() => {
     this.handleChange();
   })
  }

  generateRandomString() {
   let result = '';
   let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789$%&$£';
   let charactersLength = characters.length;

    for (let i = 0; i < 10; i++ ) {
      result += characters.charAt(Math.floor(Math.random() *
        charactersLength));
    }

    return result;
  }

  handleChange() {
    for (const key of Object.entries(this.formGroups)) {
      if (key[1].invalid === true) {
        this.invalid = true;
      }
    }

   const data = {
     index: this.formIndex,
     user: this.formGroups.formGroup.getRawValue(),
     trainer: this.formGroups.trainerFormGroup.getRawValue(),
     company: this.formGroups.companyFormGroup.getRawValue(),
     invalid: this.invalid
   }

    this.changeEvent.emit(data)
  }

  handleDelete() {
   this.deleteEvent.emit(this.formIndex);
  }

  createCompany() {
    let company = this.formGroups.companyFormGroup.getRawValue().company;

  }
}
