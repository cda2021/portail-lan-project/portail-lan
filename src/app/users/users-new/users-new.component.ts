import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user/user.service";
import {Router} from "@angular/router";
import {AlertController} from "@ionic/angular";

@Component({
  selector: 'app-users-new',
  templateUrl: './users-new.component.html',
  styleUrls: ['./users-new.component.scss']
})
export class UsersNewComponent implements OnInit {

  userForms = [];
  users: any = [];
  companies: any = [];

  constructor(private service: UserService, private router: Router, private alertController: AlertController) { }

  ngOnInit() {
    this.userForms.push({
    })
  }

  addForm() {
    this.userForms.push({})
    this.users.push({})
  }

  handleChange(data: any) {
    this.users[data.index] = data;
  }

  handleRemove(formIndex) {
    this.userForms = this.userForms.filter((form, index) => index !== formIndex)
    this.users = this.users.filter((user, index) => index !== formIndex)
  }

  handleSubmit() {
    if (this.users.length > 0) {
      this.service.registerStudents(this.users).then(() => {
        this.router.navigate(['/users'])
      })
    } else {
      this.presentAlert()
    }
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Une erreur est survenue',
      message: 'Vous devez créer au moins un utilisateur',
      buttons: ['OK']
    });

    await alert.present();
  }
}
