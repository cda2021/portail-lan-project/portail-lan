import { Component, OnInit } from '@angular/core';
import {UserService} from "../services/user/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users = [];

  constructor(private service: UserService) { }

  ngOnInit() {
    this.service.all().then((users) => this.users = users)
  }

  handleRemove(id) {
    this.service.delete(id).then(() => {
      this.users = this.users.filter((user) => user.id !== id)
    })
  }

  notMe(id) {
    return localStorage.getItem("user-id") !== id;
  }
}
