export function prepareAuthenticatedHeaders() {
  let headers = prepareHeaders();
  headers.append('Authentication-Token', localStorage.getItem('authentication-token'))

  return headers;
}

export function prepareHeaders() {
  let headers = new Headers();
  headers.append("Content-Type", "application/json");

  return headers;
}


export function preparePostParams() {
  return {
    method: "POST",
    headers: prepareHeaders(),
  }
}


export function prepareGetParams() {
  return {
    method: "GET",
    headers: prepareHeaders(),
  }
}


export function prepareDeleteParams() {
  return {
    method: "DELETE",
    headers: prepareHeaders(),
  }
}
